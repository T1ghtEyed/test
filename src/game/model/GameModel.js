import Grid from './GridClass';

class GameModel {
    constructor() {
        const ROWS = 11;
        const COLS = 7;
        const EL_COUNT = 11;

        this._rows = ROWS;
        this._cols = COLS;
        this._elCnt = EL_COUNT;
        this.grid = new Grid(COLS, ROWS, EL_COUNT);

        this.score = 0;

        console.log('MVP -> MODEL IS READY');

        this.droppedCells = [];
    }

    shuffleGrid() {
        this.grid = Grid.shuffle(this.grid, this._rows, this._cols, this._elCnt);

        while (this.findLines()) {
            this.grid = Grid.shuffle(this.grid, this._rows, this._cols, this._elCnt);
        }

        return this.grid;
    }

    getPrevInRow(row, col) {
        if (this.grid[row]){
            return this.grid[row][col-1] || null;
        } else {
            return null;
        }
    }

    getPrevInCol(row, col) {
        if (this.grid[row-1]) {
            return this.grid[row-1][col] || null;
        } else {
            return null;
        }
    }

    getNextInRow(row, col) {
        if (this.grid[row]){
            return this.grid[row][col+1] || null;
        } else {
            return null;
        }
    }

    getNextInCol(row, col) {
        if (this.grid[row+1]) {
            return this.grid[row+1][col] || null;
        } else {
            return null;
        }
    }

    getDroppedCells() {
        return this.droppedCells;
    }

    clearDroppedCells() {
        this.droppedCells = [];
    }

    getGrid() {
        return this.grid;
    }

    changeGrid(prevCell, cell) {
        let buffer = this.grid[prevCell.row][prevCell.col];
        this.grid[prevCell.row][prevCell.col] = this.grid[cell.row][cell.col];
        this.grid[cell.row][cell.col] = buffer;

        return this.grid;
    }

    destroyCells(lines) {
        let destroyedCells = [];

        for (let line of lines) {
            for (let cell of line) {
                if (this.grid[cell.row][cell.col] !== null) {
                    this.score += this.grid[cell.row][cell.col];
                    this.grid[cell.row][cell.col] = null;
                    destroyedCells.push({row: cell.row, col: cell.col});
                }
            }
        }

        return destroyedCells;
    }

    dropDown() {
        let droppedCells = [];

        for (let col = 0; col < this._cols; col++) {
            let positions = 0;
            for (let row = this._rows - 1; row >= 0; row--) {
                if (this.grid[row][col] === null) { positions++; }

                if (positions !== 0 && this.grid[row][col] !== null) {
                    this.grid[row+positions][col] = this.grid[row][col];
                    this.grid[row][col] = null;

                    this.droppedCells.push({row: row, col: col, pos: positions});

                }
            }
        }
        //console.log(this.grid);
        return droppedCells;
    }

    generateCells() {
        let generatedCells = [];

        for (let row = 0; row < this._rows; row++) {
            for (let col = 0; col < this._cols; col++) {
                if (this.grid[row][col] === null) {
                    this.grid[row][col] = Grid.generateCell(this._elCnt);
                    generatedCells.push({row: row, col: col});
                }
            }
        }

        return generatedCells;
    }

    findLines() {
        let lines = [];

        //Find lines in rows
        for (let row = 0; row < this._rows; row++) {
            let line = [];
            let lineLength = 0;
            for (let col = 0; col < this._cols; col++) {
                let curr_el = this.grid[row][col];
                let next_el = this.getNextInRow(row, col);

                if (curr_el === next_el && curr_el !== null) {
                    line.push({row: row, col: col});
                    lineLength++;
                } else {
                    if (lineLength > 1) {
                        line.push({row: row, col: col});
                        lines.push(line);
                    }
                    line = [];
                    lineLength = 0;
                }
            }
        }

        //Find lines in cols
        for (let col = 0; col < this._cols; col++) {
            let line = [];
            let lineLength = 0;
            for (let row = 0; row < this._rows; row++) {
                let curr_el = this.grid[row][col];
                let next_el = this.getNextInCol(row, col);

                if (curr_el === next_el && curr_el !== null) {
                    line.push({row: row, col: col});
                    lineLength++;
                } else {
                    if (lineLength > 1) {
                        line.push({row: row, col: col});
                        lines.push(line);
                    }
                    line = [];
                    lineLength = 0;
                }
            }
        }

        if (lines.length > 0) {
            return lines;
        } else {
            return false;
        }
    }

    checkPossibilities() {

        // Possibilities in row
        for (let row = 0; row < this._rows; row++) {
            for (let col = 0; col < this._cols; col++) {
                const currEl = this.grid[row][col];
                const nextEl = this.getNextInRow(row, col);
                const nextElx2 = this.getNextInRow(row, col+1);

                if (currEl === nextEl) {
                    const left = this.getSurroundings({row, col: col-1}, 'right');
                    const right = this.getSurroundings({row, col: col+2}, 'left');

                    if (left.indexOf(currEl) !== -1 || right.indexOf(currEl) !== -1) return true;
                }

                if (currEl === nextElx2) {
                    const center = this.getSurroundings({row, col: col+1}, 'left/right');
                    if (center.indexOf(currEl) !== -1) return true;
                }
            }
        }

        // Possibilities in col
        for (let col = 0; col < this._cols; col++) {
            for (let row = 0; row < this._rows; row++) {
                const currEl = this.grid[row][col];
                const nextEl = this.getNextInCol(row, col);
                const nextElx2 = this.getNextInCol(row+1, col);

                if (currEl === nextEl) {
                    const top = this.getSurroundings({row: row-1, col}, 'bottom');
                    const bottom = this.getSurroundings({row: row+2, col}, 'top');

                    if (top.indexOf(currEl) !== -1 || bottom.indexOf(currEl) !== -1) return true;
                }

                if (currEl === nextElx2) {
                    const center = this.getSurroundings({row: row+1, col}, 'top/bottom');
                    if (center.indexOf(currEl) !== -1) return true;
                }
            }
        }

        return false;
    }

    getSurroundings(target, exclude) {
        const top = this.getPrevInCol(target.row, target.col);
        const left = this.getPrevInRow(target.row, target.col);
        const bottom = this.getNextInCol(target.row, target.col);
        const right = this.getNextInRow(target.row, target.col);

        switch (exclude) {
            case 'right':
                return [top, left, bottom];
            case 'left':
                return [top, bottom, right];
            case 'left/right':
                return [top, bottom];
            case 'top':
                return [left, bottom, right];
            case 'bottom':
                return [top, left, right];
            case 'top/bottom':
                return [left, right];
        }
    }

}

export default  GameModel;