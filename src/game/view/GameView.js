import * as core from './../../../../web-core';
import GamePresenter from './../presenter/GamePresenter';

class GameView extends core.State{
    constructor() {
        const CELL_SIZE = 50;

        super('Game View');
        this.cellSize = CELL_SIZE;
        this.res = core.ResourceManager.instance().findResource('Test');

        this.explosions = [
            helper.createTexture(this.res, 'SparksExplosion'),
            helper.createTexture(this.res, 'SmallExplosion'),
            helper.createTexture(this.res, 'BigExplosion'),
            helper.createTexture(this.res, 'NukeExplosion')
        ];

        this.tileAnims = [
            helper.createTexture(this.res, 'Goon'),
            helper.createTexture(this.res, 'Crook'),
            helper.createTexture(this.res, 'Necromancer'),
            helper.createTexture(this.res, 'Bullet'),
            helper.createTexture(this.res, 'Bubred'),
            helper.createTexture(this.res, 'Guide'),
            helper.createTexture(this.res, 'Friffle'),
            helper.createTexture(this.res, 'Hunter'),
            helper.createTexture(this.res, 'Marine'),
            helper.createTexture(this.res, 'Rogue'),
            helper.createTexture(this.res, 'Robot'),
            helper.createTexture(this.res, 'Sreaper')
        ];

        this.presenter = new GamePresenter(this);

        console.log('MVP -> VIEW IS READY');
    }

    start() {
        console.log('StateManager -> ENTERING GAME STATE');
    }

    stop() {
        console.log('StateManager -> LEAVING GAME STATE');
    }

    drawBoard(grid) {
        const rows = grid.length;
        const cols = grid[0].length;
        this.cellsMatrix = [];

        this.board = new PIXI.Container();

        let background = new PIXI.Graphics();
        background.beginFill(0x000000);
        background.drawRect(0, 0, this.cellSize * cols, this.cellSize * rows);
        background.endFill();
        this.board.addChild(background);

        for (let row = 0; row < rows; row++) {
            this.cellsMatrix[row] = [];
            for (let col = 0; col < cols; col++) {
                this.addCell(row, col, grid, true);
            }
        }

        this.addChild(this.board);
    }

    printScore(score) {
        if (this.score) this.score.destroy();

        const scoreRect = new PIXI.Graphics();
        scoreRect.lineStyle(1, 0x000000, 1, 0.5);
        scoreRect.drawRect(0, 0, 300, 50);
        scoreRect.position.set(window.innerWidth - 150, 300);


        const scoreText = new PIXI.Text('Score: ' + score, {fontFamily : 'Arial', fontSize: 24, fill : 0x000000, align : 'center'});
        scoreRect.addChild(scoreText);

        this.score = this.addChild(scoreRect);
    }

    addCell(row, col, grid, isNew) {
        const cell = new PIXI.Graphics();
        this.cellsMatrix[row][col] = cell;
        cell.drawRect(0, 0, this.cellSize, this.cellSize);

        if (isNew) {
            cell.position.set(col * this.cellSize, row * this.cellSize);
        } else {
            cell.position.set(col * this.cellSize, row * this.cellSize - 1000);
        }

        cell.interactive = true;
        cell.buttonMode = true;

        cell.on('click', (evt) => {
            this.presenter.moveCell(evt);
        });
        cell.on('tap', (evt) => {
            this.presenter.moveCell(evt);
        });

        const background = helper.createSprite(this.res, 'TilesBack', cell);
        background.setColumn(Math.floor(Math.random()*3))
        background.width = this.cellSize;
        background.height = this.cellSize;

        const tile = new PIXI.Sprite();
        cell.addChild(tile);

        const texture = this.tileAnims[grid[row][col]];
        tile.setResAnim(texture);
        tile.addTween(new PIXI.TweenAnim(texture, {duration: 700, repeat: -1}));

        // tile.width = this.cellSize;
        // tile.height = this.cellSize;
        tile.scale.y = this.cellSize/tile.height;
        tile.scale.x = this.cellSize/tile.width;

        const scale = Math.min(tile.scale.y, tile.scale.x) - 0.3;
        tile.scale = {x: scale, y: scale};

        this.board.addChild(cell);
    }

    changeCellMatrix(prevCell, cell) {
        const buffer = this.cellsMatrix[prevCell.row][prevCell.col];
        this.cellsMatrix[prevCell.row][prevCell.col] = this.cellsMatrix[cell.row][cell.col];
        this.cellsMatrix[cell.row][cell.col] = buffer;
    }
}
export default  GameView;