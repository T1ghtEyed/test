import GameModel from './../model/GameModel';

class GamePresenter {
    constructor(view) {
        this.view = view;
        this.model = new GameModel();
        this.prevCell= null;
        this.prevTarget = null;
        this.active = true;

        console.log('MVP -> PRESENTER IS READY');
        this.grid = this.model.getGrid();
        this._rows = this.model._rows;
        this._cols = this.model._cols;

        this.view.drawBoard(this.grid);
        this.view.printScore(this.model.score);
        while (!this.model.checkPossibilities()) {
            this.shuffleGrid();
        }
    }

    shuffleGrid() {
        this.grid = this.model.shuffleGrid();
        this.view.board.destroy();
        this.view.drawBoard(this.grid);
        console.log('shuffle');
    }

    moveCell(evt) {
        let target = evt.currentTarget;
        let multiply = 0;
        const duration = 100;

        this.cell = {
            x: target.transform.position.x,
            y: target.transform.position.y,
            row: target.transform.position.y / this.view.cellSize,
            col: target.transform.position.x / this.view.cellSize,
        };

        const canMove = this.active && this.prevCell &&
            this.prevTarget &&
            (((this.cell.row === this.prevCell.row) &&
            (this.cell.col === this.prevCell.col-1 ||
            this.cell.col === this.prevCell.col+1)) ||
            ((this.cell.col === this.prevCell.col) &&
            (this.cell.row === this.prevCell.row-1 ||
            this.cell.row === this.prevCell.row+1)));



        if (canMove) {
            const switchAnim = target.addTween(new PIXI.Tween({x: this.prevCell.x, y: this.prevCell.y}, {duration}));
            this.prevTarget.addTween(new PIXI.Tween({x:this.cell.x, y: this.cell.y}, {duration}));

            this.grid = this.model.changeGrid(this.prevCell, this.cell);
            let timeout = 0;

            switchAnim.addDoneCallback(() => {
                let lines = this.model.findLines();
                let destroyedCells;

                if (!lines) {
                    target.addTween(new PIXI.Tween({x: this.cell.x, y: this.cell.y}, {duration}));
                    this.prevTarget.addTween(new PIXI.Tween({x:this.prevCell.x, y: this.prevCell.y}, {duration}));
                    this.grid = this.model.changeGrid(this.cell, this.prevCell);
                } else {
                    this.active = false;
                    this.grid = this.model.getGrid();
                    this.view.changeCellMatrix(this.prevCell, this.cell);

                    (function affectCells(lines, self) {
                        setTimeout(() => {
                            destroyedCells = self.model.destroyCells(lines);

                            let explosions = [];

                            destroyedCells.forEach((cell) => {
                                explosions.push(self.view.cellsMatrix[cell.row][cell.col].children[0].addTween(new PIXI.TweenAnim(self.view.explosions[multiply], {duration: 500})));
                            });

                            explosions[0].addDoneCallback(() => {
                                destroyedCells.forEach((cell) => {
                                    self.view.cellsMatrix[cell.row][cell.col].destroy();
                                    self.view.cellsMatrix[cell.row][cell.col] = null;
                                });
                                self.view.printScore(self.model.score);

                                self.model.dropDown();

                                self.model.getDroppedCells().forEach((cell) => {
                                    let animCell = self.view.cellsMatrix[cell.row][cell.col];
                                    let newPos = {
                                        x: animCell.transform.position.x,
                                        y: animCell.transform.position.y + self.view.cellSize * cell.pos
                                    };

                                    self.view.cellsMatrix[cell.row+cell.pos][cell.col] = animCell;
                                    animCell.addTween(new PIXI.Tween({x: newPos.x, y: newPos.y}, {duration: duration*10}));
                                    self.view.cellsMatrix[cell.row][cell.col] = null;
                                });

                                const generatedCells = self.model.generateCells();
                                self.grid = self.model.getGrid();

                                generatedCells.forEach((cell) => {
                                    if (self.view.cellsMatrix[cell.row][cell.col] === null) {
                                        self.view.addCell(cell.row, cell.col, self.grid, false);

                                        self.view.cellsMatrix[cell.row][cell.col].addTween(
                                            new PIXI.Tween(
                                                {x: cell.col * self.view.cellSize, y: cell.row * self.view.cellSize},
                                                {duration: duration*10}
                                            )
                                        );
                                    }
                                });

                                destroyedCells = [];

                                self.model.clearDroppedCells();
                                lines = self.model.findLines();

                                if (lines) {
                                    timeout = duration*20;
                                    if (multiply < 3) multiply++;
                                    affectCells(lines, self);
                                } else {
                                    while (!self.model.checkPossibilities()) {
                                        self.shuffleGrid();
                                    }
                                    multiply = 0;
                                    self.active = true;
                                }
                            });
                        }, timeout);
                    })(lines, this);

                }
                this.prevCell= null;
                this.prevTarget = null;
                timeout = 0;
            });

        } else {
            this.prevCell= this.cell;
            this.prevTarget = target;
        }
    }
}

export default  GamePresenter;