import * as core from './../../web-core';

global.core = core;
global.helper = core.helper;

import States from './States';
import GameView from './game/view/GameView';

const application = new PIXI.Application({resizeTo: 'Window'});

application.renderer.view.style.display = 'block';
application.renderer.view.style.position = 'absolute';
application.renderer.autoResize = true;
application.renderer.resize(window.innerWidth, window.innerHeight);
application.renderer.backgroundColor = 0xFFFFFF;

PIXI.settings.SCALE_MODE = 1;

window.onresize = function() {
    application.renderer.resize(window.innerWidth, window.innerHeight);
};

document.body.appendChild(application.view);

core.StateManager.instance().addStateType(States.GAME, GameView);

core.StateManager.instance().setScene(application.stage);

application.ticker.add(() => core.StateManager.instance().update());

function startGame() {
    console.log('ResourceManager -> RESOURCES LOADED');
    core.StateManager.instance().setState(States.GAME, []);
}

function loadResources() {
    core.ResourceManager.instance().addResource('Test');
    core.ResourceManager.instance().loadXml('Test', 'resources/xmls/text.xml', startGame, loader => console.info('Loading... ', loader.progress, '%'));
}

loadResources();
