import * as core from './../../../web-core';
import States from '../States';

class MainState extends core.State {
    constructor() {
        super('Main State');

        this.res = core.ResourceManager.instance().findResource('Test');

        // Маска
        const mask = new PIXI.Graphics();
        mask.drawRect(window.innerWidth / 2 - 50, window.innerHeight / 2 - 50, 100, 100);

        // Background
        const background = new PIXI.Graphics();
        background.beginFill(0xA4C2F2);
        background.drawRect(0, 0, window.innerWidth, window.innerHeight);
        background.endFill();

        this.addChild(background);

        // Спрайт пикчи ProfilePicture.png
        const test = helper.createTexture(this.res, 'ProfilePicture');
        const coin = helper.createTexture(this.res, 'CoinAnim');
        this.profilePicture = helper.createSprite(this.res, 'ProfilePicture', this, {x: window.innerWidth / 2, y: window.innerHeight / 2}, {x: 0.5, y: 0.5});
        //this.profilePicture.mask = mask;
        
        // Кнопка
        this.button = new core.Button(this.res, 'ButtonIdle', 'ButtonDown');
        this.button.position.set(window.innerWidth / 2 - this.button.width / 2, window.innerHeight - 50);
        this.button.on(core.ButtonEvent.BUTTON_DOWN, () => {
            const duration = 1000;
            const easing = PIXI.TweenEasing.outBounce;
            const pos = {
                x: 500,
                y: 100,
            };

            //this.profilePicture.addTween(new PIXI.Tween({x: pos.x, y: pos.y}, {duration, easing}));
            this.profilePicture.addTween(new PIXI.TweenAnim(coin, {duration}));
        });
        this.addChild(this.button);

        this.button_state = new core.Button(this.res, 'ButtonIdle', 'ButtonDown');
        this.button_state.position.set(window.innerWidth - this.button_state.width, window.innerHeight - 50);
        this.button_state.on(core.ButtonEvent.BUTTON_DOWN, () => {
            core.StateManager.instance().setState(States.ANIM, []);
        });
        this.addChild(this.button_state)
    }

    start() {
        console.log('ENTER MAIN STATE');
    }

    stop() {
        console.log('LEAVING MAIN STATE');
    }
}

export default MainState;